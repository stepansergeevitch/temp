import json
import sys
from base64 import b64encode
from io import BytesIO
from os import path as osp

import flask.json
from flask import Flask, abort, jsonify, make_response, request

sys.path.append(osp.abspath(osp.join(osp.realpath(__file__), '..', '..', '2-dms')))  # dirty
from dms import Database, Table, MoneyAmount, MoneyInvl


class JsonEncoder(flask.json.JSONEncoder):
    def default(self, obj):
        try:
            if isinstance(obj, (Table, Table.Column)):
                return obj.__dict__
            elif isinstance(obj, BytesIO):
                return b64encode(obj.getvalue()).decode('utf-8')
            elif type(obj) in (MoneyAmount, MoneyInvl):
                return str(obj)
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return flask.json.JSONEncoder.default(self, obj)


app = Flask(__name__)
app.json_encoder = JsonEncoder


@app.errorhandler(400)
def bad_request(error):
    return make_response(jsonify({'status': 400, 'error': 'Bad request'}), 400)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'status': 404, 'error': 'Not found'}), 404)


@app.route('/tables', methods=['GET'])
def get_tables():
    return jsonify({'status': 200, 'response': db.tablenames})


@app.route('/tables', methods=['POST'])
def create_table():
    if not request.json or 'name' not in request.json or 'cols' not in request.json:
        abort(400)

    tablename = request.json['name']
    try:
        cols = json.loads(request.json['cols'])
        db.create_table(tablename, [Table.Column(c[0], c[1]) for c in cols])
    except ValueError:
        abort(400)
    return make_response(jsonify({'status': 201, 'response': db.tables[tablename]}), 201)


@app.route('/tables/<tablename>', methods=['GET'])
def get_table(tablename):
    if tablename not in db.tablenames:
        abort(404)
    return jsonify({'status': 200, 'response': db.tables[tablename]})


@app.route('/tables/<tablename>/row/<int:row>/col/<int:col>', methods=['PUT'])
def update_value(tablename, row, col):
    if not request.json or 'value' not in request.json:
        abort(400)
    if tablename not in db.tablenames:
        abort(404)
    table = db.tables[tablename]
    if row >= len(table.data) or col >= len(table.cols):
        abort(404)
    try:
        table.update_value(row, col, request.json['value'])
    except ValueError:
        abort(400)
    return make_response(jsonify({'status': 204}), 204)


@app.route('/tables/<tablename>/remove_duplicates', methods=['POST'])
def remove_duplicates(tablename):
    if tablename not in db.tablenames:
        abort(404)
    db.tables[tablename].remove_duplicates()
    return make_response(jsonify({'status': 204}), 204)


if __name__ == '__main__':
    db = Database.load(osp.abspath(osp.join(osp.realpath(__file__), '..', 'db')))
    app.run()
    # db.save('db')
