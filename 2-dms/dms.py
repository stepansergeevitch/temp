from io import BytesIO
from numbers import Number
from pickle import Pickler, Unpickler


TYPES = ('integer', 'real', 'char', 'picture', 'moneyAmount', 'moneyInvl')


class MoneyAmount:
    maxValue = 1e13

    def __init__(self, value):
        self.value = self.fromRaw(value)

    def fromRaw(self, value):
        if isinstance(value, self.__class__):
            return value.value
        elif isinstance(value, str):
            try:
                return float(value)
            except:
                try:
                    return float(value[1:])
                except:
                    pass
        elif isinstance(value, Number):
            self.validate(float(value))
            return float(value)
        raise ValueError("bad value string")

    def validate(self,value):        
        if value < 0:
            raise ValueError("money amount can't be less than zero")
        if value > MoneyAmount.maxValue:
            raise ValueError("money amount exceded maximum")

    def __str__(self):
        return '${:.2f}'.format(self.value)

    def __eq__(self, other):
        return self.value == other.value

class MoneyInvl:

    def __init__(self, a, b):
        self.a = MoneyAmount(a)
        self.b = MoneyAmount(b)

    def __eq__(self, other):
        return type(other) == type(self) and \
        self.a == other.a and self.b == other.b

    def __str__(self):
        return '{%s, %s}' % (str(self.a), str(self.b))

class Database:
    def __init__(self):
        self.tablenames = []
        self.tables = {}

    def create_table(self, name, cols):
        if name in self.tables:
            raise ValueError('table with specified name exists in the database')
        table = Table(name, cols)
        self.tablenames.append(name)
        self.tables[name] = table
        return table

    def save(self, name):
        with open(name, 'wb') as f:
            Pickler(f).dump(self)

    @staticmethod
    def load(file, mode='filename'):
        if mode == 'filename':
            with open(file, 'rb') as f:
                return Unpickler(f).load()
        elif mode == 'stream':
            return Unpickler(file).load()


class Table:
    class Column:
        def __init__(self, name, type_):
            if type(name) is not str or len(name) == 0:
                raise ValueError('column name must be a non-empty string')
            if type_ not in TYPES:
                raise ValueError('column type %s not recognized' % type_)
            self.name = name
            self.type = type_

        def rename(self, new_name):
            self.name = new_name

        def __getitem__(self, ind):
            return ([self.name, self.type])[ind]

    def __init__(self, name, cols):

        if type(name) is not str or len(name) == 0:
            raise ValueError('column name must be a non-empty string')
        if not cols:
            raise ValueError('provide columns for the table')
        for i, col in enumerate(cols):
            if type(col) is not Table.Column:
                try:
                    cols[i] = Table.Column(*col)
                except:
                    raise ValueError('invalid column at index %d', i, col)
        self.name = name
        self.cols = cols
        self.data = []

    def add_row(self, vals):
        if type(vals) is not list:
            raise ValueError('provide a list of values for the row')
        if len(vals) != len(self.cols):
            raise ValueError('expected %d values for the row, %d provided' % (len(self.cols), len(vals)))

        vals = list(vals)  # to prevent unintended mutation
        for i in range(len(vals)):
            col = self.cols[i]
            vals[i] = self.validate_type(vals[i], col.type)
            if vals[i] is None:
                raise ValueError('value type mismatch at column %d (%s), expected %s' % (i, col.name, col.type))
        self.data.append(vals)

    def update_value(self, row, col, val):
        expected_type = self.cols[col].type
        typed_value = self.validate_type(val, expected_type)
        if typed_value is None:
            raise ValueError(expected_type)
        self.data[row][col] = typed_value
        return typed_value

    @staticmethod
    def validate_type(val, expected_type):
        if expected_type == 'integer':
            if type(val) is int:
                return val
            try:
                return int(val)
            except ValueError:
                return None

        elif expected_type == 'real':
            if type(val) is float:
                return val
            try:
                return float(val)
            except ValueError:
                return None

        elif expected_type == 'char':
            return val if type(val) is str and len(val) == 1 else None

        elif expected_type == 'picture':
            return val if isinstance(val, BytesIO) else None

        elif expected_type == 'moneyAmount':
            try:
                return MoneyAmount(val)
            except:
                return None
        elif expected_type == 'moneyInvl':
            if isinstance(val, MoneyInvl):
                return val
            elif isinstance(val, str):
                invl = val.split(';')
                if len(invl) == 2:
                    try:
                        return MoneyInvl(*invl)
                    except:
                        return None
                else:
                    return None
            return None

    def rename_column(self, col, name):
        self.cols[col].rename(name)
