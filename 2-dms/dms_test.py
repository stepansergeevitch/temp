import unittest

from dms import Database, Table, MoneyAmount, MoneyInvl


class Testing(unittest.TestCase):
    def assertPasses(self, func, args, expected_success):
        try:
            func(*args)
            if not expected_success:
                self.fail('testcase %s succeeded despite expected failure' % str(args))
        except ValueError as ex:
            if expected_success:
                self.fail('testcase %s was expected to succeed but failed: %s' % (args, ex))

    def test_validate_type(self):
        cases = (
            ('integer', 1, 1),
            ('integer', '2', 2),
            ('integer', -54, -54),
            ('integer', -5.4, -5),
            ('integer', 'NaN', None),
            ('real', '2.5e5', 2.5e5),
            ('real', -24.99999, -24.99999),
            ('char', 'x', 'x'),
            ('char', 'RJ', None),
            ('moneyAmount', '$80', MoneyAmount(80)),
            ('moneyAmount', '33', MoneyAmount(33)),
            ('moneyAmount', 8, MoneyAmount(8)),
            ('moneyInvl', '$80;3', MoneyInvl(80, 3)),
            ('moneyInvl', '33;44', MoneyInvl(33, 44)),
        )
        for t, val, expected in cases:
            actual = Table.validate_type(val, t)
            self.assertEqual(actual, expected)

    def test_create_table(self):
        cases = (
            (('1', [Table.Column('1', 'integer'), Table.Column('2', 'real')]), True),
            (('1', [Table.Column('1', 'integer'), Table.Column('2', 'real')]), False),  # already exists
            (('', [Table.Column('1', 'integer'), Table.Column('2', 'real')]), False),
            (('2', [Table.Column('1', 'integer'), Table.Column('2', 'unreal')]), False),
            (('3', [Table.Column('', 'integer'), Table.Column('2', 'real')]), False),
            (('4', [Table.Column(50, 'integer'), Table.Column('2', 'real')]), False),
            (('5', []), False),
        )
        database = Database()
        for args, expected_success in cases:
            self.assertPasses(database.create_table, args, expected_success)

    def test_rename_col(self):
        cases = (
            (
                ('1', [Table.Column('1', 'integer')]),
                ((0, 'new_name'), ),
                ['new_name']
            ),
            (
                ('2', [Table.Column('1', 'integer'), Table.Column('2', 'real')]),
                [(0, '3'), (1, '4')],
                ['3', '4']
            ),
        )
        database = Database()
        for cols, renames, expected_cols in cases:
            table = database.create_table(*cols)
            for rename in renames:
                table.rename_column(*rename)
            actual_cols = list(map(lambda a:a.name, table.cols))
            self.assertEqual(
                actual_cols,
                expected_cols
            )


if __name__ == '__main__':
    unittest.main()
