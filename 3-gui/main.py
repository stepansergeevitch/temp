import sys
from os import path as osp

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import *

from PIL.Image import Image as Picture

sys.path.append(osp.abspath(osp.join(osp.realpath(__file__), '..', '..', '2-dms')))  # dirty
from dms import Database, Table, MoneyAmount, MoneyInvl


class DmsWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.changes_made = False
        self.current_db = None
        self.current_table = None
        self.layout_root = QVBoxLayout()
        self.menubar = self.menuBar()
        self.save_path = None
        self.statusbar = self.statusBar()
        self.tablebar = QTabBar()
        self.tableview = QTableWidget()

        self.init_ui()
        self.init_menubar()
        self.set_tableview_visibility()

    def init_ui(self):
        self.tablebar.currentChanged.connect(self.change_tab)
        self.tableview.doubleClicked.connect(self.table_doubleclick)
        self.tableview.setContextMenuPolicy(Qt.CustomContextMenu)
        self.tableview.customContextMenuRequested.connect(self.table_contextmenu)
        self.layout_root.addWidget(self.tableview)
        self.layout_root.addWidget(self.tablebar)
        self.setCentralWidget(QWidget(self))
        self.centralWidget().setLayout(self.layout_root)
        self.setGeometry(800, 600, 1600, 1200)
        self.setWindowTitle('СУТБД')
        self.setWindowIcon(QIcon('icon.png'))

    def init_menubar(self):

        menu_file = self.menubar.addMenu('&Файл')

        action_create = QAction('&Нова база', self)
        action_create.setShortcut('Ctrl+N')
        action_create.triggered.connect(self.file_create)
        menu_file.addAction(action_create)

        action_open = QAction('&Відкрити...', self)
        action_open.setShortcut('Ctrl+O')
        action_open.triggered.connect(self.file_open)
        menu_file.addAction(action_open)

        action_save = QAction('&Зберегти', self)
        action_save.setShortcut('Ctrl+S')
        action_save.triggered.connect(self.file_save)
        menu_file.addAction(action_save)

        action_saveas = QAction('&Зберегти як...', self)
        action_saveas.setShortcut('Ctrl+Shift+S')
        action_saveas.triggered.connect(self.file_saveas)
        menu_file.addAction(action_saveas)

        action_exit = QAction('&Вихід', self)
        action_exit.setShortcut('Ctrl+Q')
        action_exit.triggered.connect(qApp.quit)
        menu_file.addAction(action_exit)

        menu_db = self.menubar.addMenu('&База')

        action_create_table = QAction('&Нова таблиця', self)
        action_create_table.setShortcut('Ctrl+T')
        action_create_table.triggered.connect(self.table_create)
        menu_db.addAction(action_create_table)

        # action_remove_duplicates = QAction('&Видалити рядки-дублікати з поточної таблиці', self)
        # action_remove_duplicates.triggered.connect(self.table_remove_duplicates)
        # menu_db.addAction(action_remove_duplicates)

        menu_help = self.menubar.addMenu('&Довідка')

        action_about = QAction('&Про роботу', self)
        action_about.setShortcut('F1')
        action_about.triggered.connect(self.message_about)
        menu_help.addAction(action_about)

    def message_about(self):
        QMessageBox.information(
            self,
            'Інформація про роботу',
            'Виконав студент групи МІ-4 Бурлаков Степан'
        )

    def file_create(self):
        self.set_current_db(Database())

    def file_open(self):
        dialog_result = QFileDialog.getOpenFileName(self, 'Виберіть файл')
        if not dialog_result[0]:
            return
        filename = dialog_result[0]
        db = Database.load(filename)
        self.set_current_db(db)
        self.changes_made = False

    def file_save(self):
        if self.current_db is None:
            return
        if self.save_path is None:
            self.file_saveas()
        else:
            self.current_db.save(self.save_path)
            self.changes_made = False

    def file_saveas(self):
        if self.current_db is None:
            return
        dialog_result = QFileDialog.getSaveFileName(self, 'Зберегти')
        if not dialog_result[0]:
            return
        self.save_path = dialog_result[0]
        self.file_save()

    def tablebar_set(self, tablenames=None):
        while self.tablebar.count() > 0:
            self.tablebar.removeTab(0)
        if tablenames is not None:
            for tablename in tablenames:
                self.tablebar.addTab(tablename)
            self.tablebar.setCurrentIndex(0)  # change_tab is fired automatically

    def tablebar_add(self, tablename):
        self.tablebar.addTab(tablename)
        self.tablebar.setCurrentIndex(self.tablebar.count() - 1)

    def set_current_db(self, db):
        self.current_db = db
        self.set_tableview_visibility()
        if db is None:
            self.tablebar_set()
            return
        tablenames = list(db.tables.keys())
        self.tablebar_set(tablenames)

    def set_tableview_visibility(self):
        if self.current_db is None:
            self.tableview.setVisible(False)
            self.tablebar.setVisible(False)
            self.statusbar.showMessage('Для початку роботи відкрийте або створіть базу даних.')
        else:
            self.tableview.setVisible(True)
            self.tablebar.setVisible(True)

    def change_tab(self):
        tablename = self.tablebar.tabText(self.tablebar.currentIndex())
        self.table_load(tablename)

    def table_load(self, tablename):
        table = self.current_db.tables[tablename]
        self.tableview.clear()
        self.tableview.setRowCount(len(table.data))
        self.tableview.setColumnCount(len(table.cols))
        self.tableview.setHorizontalHeaderLabels([col[0] for col in table.cols])
        for i, row in enumerate(table.data):
            for j, val in enumerate(row):
                item = QTableWidgetItem(str(val))
                item.setFlags(Qt.ItemIsEnabled | Qt.ItemIsSelectable)
                self.tableview.setItem(i, j, item)
        self.current_table = table
        self.statusbar.showMessage('Таблицю "%s" завантажено.' % tablename)

    def table_doubleclick(self, model_index):
        row, col = model_index.row(), model_index.column()
        val = str(self.current_table.data[row][col])
        new_val, success = QInputDialog.getText(self, 'Редагування поля', 'Введіть нове значення:', text=val)
        if not success:
            return
        try:
            typed_value = self.current_table.update_value(row, col, new_val)
        except ValueError as e:
            QMessageBox.critical(self, 'Помилка', 'Тип значення не відповідає очікуваному (%s).' % str(e))
            return
        self.tableview.item(row, col).setText(str(typed_value))
        self.changes_made = True
        self.statusbar.showMessage('Значення успішно оновлено.')

    def table_create(self):
        if self.current_db is None:
            return
        data, success = TableDialog.call(self)
        if not success:
            return
        self.current_db.create_table(*data)
        self.tablebar_add(data[0])

    def table_contextmenu(self, pos):
        menu = QMenu()
        action_add = menu.addAction("&Додати рядок")

        action = menu.exec_(self.tableview.mapToGlobal(pos))
        if action == action_add:
            self.table_add_row()

    def table_add_row(self):
        if self.current_table is None:
            return
        data, success = AddRowDialog.call(tuple(col[0] for col in self.current_table.cols), self)
        if not success:
            return
        # TODO move this logic to AddRowDialog.call()
        try:
            self.current_table.add_row(data)
        except ValueError:
            QMessageBox.critical(self, 'Помилка', 'Рядок не може бути додано, перевірте типи значень.')
            return
        row = len(self.current_table.data) - 1
        self.tableview.setRowCount(row + 1)
        for col, datum in enumerate(self.current_table.data[row]):
            item = QTableWidgetItem(str(datum))
            item.setFlags(Qt.ItemIsEnabled | Qt.ItemIsSelectable)
            self.tableview.setItem(row, col, item)
        self.changes_made = True
        self.statusbar.showMessage('Рядок успішно додано.')

    def table_remove_duplicates(self):
        if self.current_table == None:
            return
        result = self.current_table.remove_duplicates()
        if not result:
            QMessageBox.information(self, 'Інформація', 'Дублікатів не знайдено.')
            return
        for delta, ind in enumerate(result):  # delta to compensate for index difference after deletions
            self.tableview.removeRow(ind - delta)
        self.statusbar.showMessage('Дублікати успішно видалено.')


class TableDialog(QDialog):
    def __init__(self, parent):
        super().__init__(parent)

        layout_root = QVBoxLayout(self)

        layout_name = QHBoxLayout()
        layout_name.addWidget(QLabel('Назва:', self))
        self.name_editview = QLineEdit(self)
        layout_name.addWidget(self.name_editview)
        layout_root.addLayout(layout_name)

        layout_cols = QVBoxLayout()
        layout_cols.addWidget(QLabel('Стовпчики:', self))
        layout_col_toolbar = QHBoxLayout()
        self.col_name_editview = QLineEdit(self)
        self.col_name_editview.setPlaceholderText('Назва')
        layout_col_toolbar.addWidget(self.col_name_editview)
        self.col_type_comboview = QComboBox(self)
        self.col_type_comboview.addItems(['integer', 'real', 'char', 'picture', 'moneyAmount', 'moneyInvl'])
        layout_col_toolbar.addWidget(self.col_type_comboview)
        button_add = QPushButton('+', self)
        button_add.clicked.connect(self.add_col)
        layout_col_toolbar.addWidget(button_add)
        button_remove = QPushButton('-', self)
        button_remove.clicked.connect(self.remove_col)
        layout_col_toolbar.addWidget(button_remove)
        layout_cols.addLayout(layout_col_toolbar)
        self.cols_tableview = QTableWidget(self)
        self.cols_tableview.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.cols_tableview.setColumnCount(2)
        self.cols_tableview.setHorizontalHeaderLabels(['Назва', 'Тип'])
        layout_cols.addWidget(self.cols_tableview)
        layout_root.addLayout(layout_cols)

        buttons = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel, Qt.Horizontal, self)
        buttons.accepted.connect(self.accept)
        buttons.rejected.connect(self.reject)
        layout_root.addWidget(buttons)

    def add_col(self):
        name = self.col_name_editview.text()
        if not name:
            QMessageBox.critical(self, 'Помилка', 'Вкажіть ім\'я для стовпчика.')
            return
        for i in range(self.cols_tableview.rowCount()):
            if self.cols_tableview.item(i, 0).text() == name:
                QMessageBox.critical(self, 'Помилка', 'Стовпчик з таким іменем уже додано.')
                return
        row = self.cols_tableview.rowCount()
        self.cols_tableview.insertRow(row)
        self.cols_tableview.setItem(row, 0, QTableWidgetItem(name))
        self.cols_tableview.setItem(row, 1, QTableWidgetItem(self.col_type_comboview.currentText()))

    def remove_col(self):
        selection = self.cols_tableview.selectedItems()
        for index in range(0, len(selection), self.cols_tableview.columnCount()):
            self.cols_tableview.removeRow(self.cols_tableview.row(selection[index]))

    def get_data(self):
        name = self.name_editview.text()
        cols = [(self.cols_tableview.item(i, 0).text(), self.cols_tableview.item(i, 1).text())  \
                for i in range(self.cols_tableview.rowCount())]
        return name, cols

    @staticmethod
    def call(parent=None):
        dialog = TableDialog(parent)
        result = dialog.exec_()
        data = dialog.get_data()
        return data, result == QDialog.Accepted


class AddRowDialog(QDialog):
    def __init__(self, fields, parent):
        super().__init__(parent)

        layout_root = QVBoxLayout(self)
        self.field_edits = []

        for field in fields:
            layout_field = QHBoxLayout()
            layout_field.addWidget(QLabel(field + ':', self))
            field_edit = QLineEdit(self)
            layout_field.addWidget(field_edit)
            self.field_edits.append(field_edit)
            layout_root.addLayout(layout_field)

        buttons = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel, Qt.Horizontal, self)
        buttons.accepted.connect(self.accept)
        buttons.rejected.connect(self.reject)
        layout_root.addWidget(buttons)

    def get_data(self):
        result = []
        for field in self.field_edits:
            if field.text():
                result.append(field.text())
            else:
                QMessageBox.critical(self, 'Помилка', 'Заповніть усі поля.')
                return
        return result

    @staticmethod
    def call(fields, parent=None):
        if not fields:
            raise ValueError('fields must be a valid list of strings')
        dialog = AddRowDialog(fields, parent)
        result = dialog.exec_()
        data = dialog.get_data()
        return data, result == QDialog.Accepted

if __name__ == '__main__':
    app = QApplication(sys.argv)

    window = DmsWindow()
    window.show()

    sys.exit(app.exec_())
